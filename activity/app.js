let posts = []
let count  = 1 

// add post data
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
    
    // preventDefault allows the webpage to continue executing the codes without the need to reload
    e.preventDefault()

    posts.push({
        id : count,
        title : document.querySelector("#txt-title").value,
        synopsis : document.querySelector("#txt-body").value

    })

    count++
    // console.log(posts)
    // console.log(count)

    showPost(posts)

    alert("movie post successfully created")
})

// show post

const showPost = (posts) => {
    let postEntries = "";

    posts.forEach((post) => {
        // get each element one by one
        postEntries += `
            <div id = "post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>

                <p id="post-body-${post.id}">${post.synopsis}</p>
                <button onclick="editPost('${post.id}')">edit</button>
                <button onclick="deletePost('${post.id}')">delete</button>
            </div>
        `
    })
    document.querySelector("#movie-post-entries").innerHTML = postEntries
}

// edit / update

const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML
    let synopsis = document.querySelector(`#post-body-${id}`).innerHTML

    document.querySelector('#txt-edit-id').value = id
    document.querySelector('#txt-edit-title').value = title
    document.querySelector('#txt-edit-body').value = synopsis
}

// update post 0229464744 
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault()
    for (let i=0;i < posts.length;i++) {
        if (posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
            posts[i].title = document.querySelector('#txt-edit-title').value
            posts[i].synopsis = document.querySelector('#txt-edit-body').value

            alert('movie post updated')
            showPost(posts)
            break
        }
    }
})


/* 
Activity
Make the delete buton work

pressing the delete button will delete the post and not the whole arrayt
*/

const deletePost = (id) => {
    posts = posts.filter((post) => {
        if (post.id.toString() !== id){
            return post
        }
    })

    document.querySelector(`#post-${id}`).remove()
}


